import React, { useState } from "react";
import { Text, View, Image, SafeAreaView, StyleSheet, TextInput, StatusBar, ScrollView, Switch, TouchableWithoutFeedback} from 'react-native';

const CatApp = () => {
  const [text, onChangeText] = React.useState("Kotak Ini bisa kamu tulis Sesuatu!!");
  const [number, onChangeNumber] = React.useState(null);
  const [isEnabled, setIsEnabled] = useState(false);
  const toggleSwitch= () => setIsEnabled(previousState => !previousState);
  const [count, setCount] = useState(0);
  const onPress = () => {setCount(count + 1);};

  return (
    <SafeAreaView style={styles.container}>
      <ScrollView style={styles.scrollView}>
      
      <View style={styles.container}>
              <Text style={styles.title}>Ini Adalah Tugas Apmob</Text>
     </View>

     <View style={styles.CountPress}>
        <View style={styles.countContainer}>
          <Text style={styles.countText}>Terhitung: {count}</Text>
        </View>
        <TouchableWithoutFeedback onPress={onPress}>
          <View style={styles.button}>
          <Text style={styles.TouchText}>Pencet Disini</Text>
          </View>
          </TouchableWithoutFeedback>
      </View>

      <View style={styles.switchbar}>
         <Switch
           trackColor={{ false: "red", true: "blue" }}
           thumbColor={isEnabled ? "white" : "black"}
           ios_backgroundColor="blue"
           onValueChange={toggleSwitch}
           value={isEnabled}
          />
      </View>

      <View style={{
        flex: 1,
        justifyContent: 'flex-start',
        alignItems: 'center'
      }}>
          <TextInput
            style={styles.input}
            onChangeText={onChangeText}
           value={text}
         />
        <TextInput
            style={styles.input}
            onChangeText={onChangeNumber}
            value={number}
            placeholder="Isi Aku dong, Apa aja boleh Yang penting Di isi, pake angka yaa!!!"
            keyboardType="numeric"
          />
      
      
        <Image
          source={{uri: "https://cdn.cloudflare.steamstatic.com/steam/apps/1085660/capsule_616x353.jpg?t=1631638993"}}
          style={{width: 200, height: 200, marginTop:20, marginBottom:20}}
        />
        <Image
          source={{uri: "https://cdn.cloudflare.steamstatic.com/steam/apps/1085660/capsule_616x353.jpg?t=1631638993"}}
          style={{width: 200, height: 200, marginTop:20, marginBottom:20}}
        />
        <Image
          source={{uri: "https://cdn.cloudflare.steamstatic.com/steam/apps/1085660/capsule_616x353.jpg?t=1631638993"}}
          style={{width: 200, height: 200, marginTop:20, marginBottom:20}}
        />
        <Image
          source={{uri: "https://cdn.cloudflare.steamstatic.com/steam/apps/1085660/capsule_616x353.jpg?t=1631638993"}}
          style={{width: 200, height: 200, marginTop:20, marginBottom:20}}
        />
        <Image
          source={{uri: "https://cdn.cloudflare.steamstatic.com/steam/apps/1085660/capsule_616x353.jpg?t=1631638993"}}
          style={{width: 200, height: 200, marginTop:20, marginBottom:20}}
        />
        <Image
          source={{uri: "https://cdn.cloudflare.steamstatic.com/steam/apps/1085660/capsule_616x353.jpg?t=1631638993"}}
          style={{width: 200, height: 200, marginTop:20, marginBottom:20}}
        />
        <Image
          source={{uri: "https://cdn.cloudflare.steamstatic.com/steam/apps/1085660/capsule_616x353.jpg?t=1631638993"}}
          style={{width: 200, height: 200, marginTop:20, marginBottom:20}}
        />
        <Text >Come Play With me now</Text>
      </View>
      
     </ScrollView>
    </SafeAreaView>
  );
};
const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: StatusBar.currentHeight,
    flex: 2,
    alignItems: "center",
    justifyContent: "center",
    flex: 3,
    backgroundColor: "#eaeaea",
    
  },
  CountPress:{
    flex: 1,
    justifyContent: "center",
    paddingHorizontal: 10
  },
  TouchText: {
    color: "white"
  },
  button: {
    alignItems: "center",
    backgroundColor: "black",
  padding: 10
  },
  countContainer: {
    alignItems: "center",
    padding: 10
  },
  countText: {
    color: "#FF00FF"
  },
  switchbar: {
    flex: 2,
    alignItems: "center",
    justifyContent: "center"
  },
  scrollView: {
    backgroundColor: '#dddddd',
    marginHorizontal: 5,
  },
  title: {
    marginTop: 1,
    paddingVertical: 8,
    borderWidth: 4,
    borderColor: "#20232a",
    borderRadius: 1,
    backgroundColor: "#61dafb",
    color: "#20232a",
    textAlign: "center",
    fontSize: 30,
    fontWeight: "bold"
  },
  text: {
    fontSize: 42,
  },
  input: {
    height: 40,
    margin: 12,
    borderWidth: 1,
    padding: 10,
  },
});

export default CatApp;